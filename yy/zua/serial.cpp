#include "serial.h"
#include "ui_serial.h"

Serial::Serial(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Serial)
{
    ui->setupUi(this);
    sysIint();
}

Serial::~Serial()
{
    delete ui;
}
// 获取串口配置 打开串口
bool Serial::getSerialPortConfig()
{
    //    获取串口配置
    QString mPortName = ui->cmd_port_name->currentText();//     端口号
    QString mBaudRate = ui->cmd_baud_rate->currentText();//     波特率
    QString mDataBits = ui->cmd_Data_Bits->currentText();//     数据位
    QString mParity = ui->cmd_parity->currentText();//     校验位
    QString mStopBits = ui->cmd_Stop_Bits->currentText();//      停止位

    //     设置串口
    //     端口号
    mSerialPort.setPortName(mPortName);
    //     波特率
    mSerialPort.setBaudRate(mBaudRate.toInt());
    /*if("9600" == mBaudRate)
    {
        mSerialPort.setBaudRate(QSerialPort::Baud9600);
    }
    else if("115200" == mBaudRate)
    {
        mSerialPort.setBaudRate(QSerialPort::Baud115200);
    }
    else
    {
        mSerialPort.setBaudRate(QSerialPort::Baud19200);
    }*/

    //     数据位
    if("5" == mDataBits)
    {
       mSerialPort.setDataBits(QSerialPort::Data5);
    }
    else if("6" == mDataBits)
    {
       mSerialPort.setDataBits(QSerialPort::Data6);
    }
    else if("7" == mDataBits)
    {
        mSerialPort.setDataBits(QSerialPort::Data7);
    }
    else
    {
        mSerialPort.setDataBits(QSerialPort::Data8);
    }

    //     校验位

    if("Even" == mParity)
    {
       mSerialPort.setParity(QSerialPort::EvenParity);// 偶数
    }
    else if ("ODD" == mParity)
    {
      mSerialPort.setParity(QSerialPort::OddParity);//奇数的
    }
    else
    {
      mSerialPort.setParity(QSerialPort::NoParity);
    }

    //      停止位
    if("1.5" == mStopBits)
    {
        mSerialPort.setStopBits(QSerialPort::OneAndHalfStop);
    }
    else if("2" == mStopBits)
    {
        mSerialPort.setStopBits(QSerialPort::TwoStop);
    }
    else
    {
        mSerialPort.setStopBits(QSerialPort::OneStop);
    }

    //      打开串口
    return mSerialPort.open(QSerialPort::ReadWrite);
}
//系统初始化
void Serial::sysIint()
{
    ui->cmd_port_name->clear();
    this->setWindowTitle("xxx的串口调试GUI");//窗口名字
    connect(&mSerialPort,&QSerialPort::readyRead,this,&Serial::on_SerialPort_readyRead);//XI
    mIsOpen = false;
    ui->BtnSend->setEnabled(mIsOpen);//没连接串口按键为灰色

    //    智能识别当前系统的有效串口号
    //通过QSerialPortInfo查找可用串口
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        //将可用串口添加到端口显示框
        ui->cmd_port_name->addItem(info.portName());
    }
    //timer 自动发送
    timer = new QTimer(this);
    connect(timer,&QTimer::timeout,[=](){
        emit my_send_signals(true); //触发发送信号
    });

    //QToolBar *toolBar1= addToolBar("工具栏");//QToolBar工具栏
    //发送信号   发送槽函数
    connect(this,&Serial::my_send_signals,this,&Serial::on_BtnSend_clicked);
}
//字符串转16进制
QByteArray Serial::QString2Hex(QString str)
{
    QByteArray senddata;
    int hexdata,lowhexdata;
    int hexdatalen = 0;
    int len = str.length();

    senddata.resize(len/2);
    char lstr,hstr;

    for(int i=0; i<len; )
    {
        hstr=str[i].toLatin1();
        if(hstr == ' ')
        {
            i++;
            continue;
        }
        i++;
        if(i >= len)
            break;
        lstr = str[i].toLatin1();
        hexdata = ConvertHexChar(hstr);
        lowhexdata = ConvertHexChar(lstr);
        if((hexdata == 16) || (lowhexdata == 16))
            break;
        else
            hexdata = hexdata*16+lowhexdata;
        i++;
        senddata[hexdatalen] = (char)hexdata;
        hexdatalen++;
    }
    senddata.resize(hexdatalen);
    return senddata;
}
//字符转16进制
char Serial::ConvertHexChar(char ch)
{
    if((ch >= '0') && (ch <= '9'))
        return ch-0x30;
    else if((ch >= 'A') && (ch <= 'F'))
        return ch-'A'+10;
    else if((ch >= 'a') && (ch <= 'f'))
        return ch-'a'+10;
    else return (-1);
}

/*-----------------slot--------------------*/
// 准备接收串口数据槽
void Serial::on_SerialPort_readyRead()
{
    QByteArray recvData = mSerialPort.readAll();//从串口读取数据
    QString str_rev;
    if(ui->chk_rev_hex->checkState() == Qt::Checked){   //HEX 16进制
            str_rev = QString(recvData.toHex(' ').toUpper().append(' '));
    }else{
            str_rev =QString::fromLocal8Bit(recvData);//处理汉语显示乱码,函数返回的是String类型的数
    }

    //文本框显示接收数据
    ui->textRecv->insertPlainText(str_rev);//显示内容，不会自动清空
    //将光标定位到最后
    ui->textRecv->moveCursor(QTextCursor::End);
}


//打开串口按键
void Serial::on_btnOpen_clicked()
{
    if(true == mIsOpen)
    {
        // 当前串口已经打开了一个串口，这时要执行关闭串口
        mSerialPort.close();
        mIsOpen = false;
        ui->BtnSend->setEnabled(mIsOpen);//发送按键不用
    }
    else
    {
        // 当前串口助手没有打开串口，这时要执行打开串口动作
        if(true == getSerialPortConfig())
        {
            mIsOpen = true;
            ui->BtnSend->setEnabled(mIsOpen);//发送按键可用
        }
        else
        {
            mIsOpen = false;
        }
    }
}
//关闭串口按键
void Serial::on_btnClose_clicked()
{
     mSerialPort.close();//执行关闭串口
     ui->BtnSend->setEnabled(false);//没连接串口按键为灰色
     mIsOpen = false;
     ui->textRecv->clear();//关闭串口，同时清楚接收区
}

//发送按键
void Serial::on_BtnSend_clicked()
{
    qDebug()<<"发送按键"<<endl;
    QString data = ui->textSend->toPlainText();
    QByteArray array;//字节数组

    //复选框  16 进制发送
    if(ui->chk_send_hex->checkState() == Qt::Checked){
        array = QString2Hex(data);  //HEX 16进制,QString转Hex QString转十六进制数,自己写的
    }else{
        array = data.toLatin1();    //ASCII，QString类提供的
    }
    mSerialPort.write(array);   //发送数据
    //发送完后，清空发送区
    ui->textSend->clear();
}
//清楚接收区按键
void Serial::on_btn_clear_clicked()
{
   qDebug()<<"清楚接收区按键"<<endl;
   ui->textRecv->clear();
}
//清空发送区按键
void Serial::on_btn_clear_send_clicked()
{
    qDebug()<<"清空发送区按键"<<endl;
    ui->textSend->clear();
}
//自动触发复选框  启动定时器和停止定时器
void Serial::on_checkBox_3_stateChanged(int arg1)
{
    if(arg1){
        timer->start(ui->spinBox->value()); //启动定时器
    }else{
        timer->stop();  //停止定时器
    }
}
