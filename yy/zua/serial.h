#ifndef SERIAL_H
#define SERIAL_H

#include <QMainWindow>
/*---port串口--*/
#include <QSerialPort>
#include <QSerialPortInfo>
/*---QString--*/
#include <QString>
/*---QTimer定时器--*/
#include <QTimer>
/*-----QToolBar工具栏------*/
#include <QToolBar>
/*---QDebug打印--*/
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class Serial; }
QT_END_NAMESPACE

class Serial : public QMainWindow
{
    Q_OBJECT

public:
    Serial(QWidget *parent = nullptr);
    ~Serial();

private:
    Ui::Serial *ui;
    /*------------变量----------------*/
    QSerialPort mSerialPort;//串口全局类声明
    bool mIsOpen;//是否打开串口
    QTimer *timer;//自动发送定时器声明

    /*------------函数----------------*/
    bool getSerialPortConfig();// 获取串口配置 打开串口
    //用户系统初始化
    void sysIint();
    //字符串转16进制
    QByteArray QString2Hex(QString str);
    //字符转16进制
    char ConvertHexChar(char ch);


signals:    //自定义信号
    //发送使能信号
    void my_send_signals(bool); //触发发送信号

private slots:
    void on_SerialPort_readyRead();// 准备接收串口数据槽，自定义的
    void on_btnOpen_clicked();//打开串口按键槽，使用转到槽
    void on_btnClose_clicked();//关闭串口按键，使用转到槽
    void on_BtnSend_clicked();//发送按键，使用转到槽
    void on_btn_clear_clicked();//清楚接收区按键，使用转到槽
    void on_btn_clear_send_clicked();//清空发送区按键，使用转到槽
    void on_checkBox_3_stateChanged(int arg1);//自动触发复选框  启动定时器和停止定时器，使用转到槽
};
#endif // SERIAL_H
